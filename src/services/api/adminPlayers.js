import axios from 'axios'

export default {
  getPlayers(params) {
    return axios.get('/project', {
      params
    })
  },
  editPlayer(id, payload) {
    return axios.patch(`/project/${id}`, payload)
  },
  savePlayer(payload) {
    return axios.post('/project/', payload)
  },
  deletePlayer(id) {
    return axios.delete(`/project/${id}`)
  }
}
