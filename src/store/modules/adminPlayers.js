import * as types from '@/store/mutation-types'
import api from '@/services/api/adminPlayers'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  players: (state) => state.players,
  totalPlayers: (state) => state.totalPlayers
}

const actions = {
  getPlayers({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getPlayers(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.PLAYERS, response.data.docs)
            commit(types.TOTAL_PLAYERS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editPlayer({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        name: payload.name,
        description: payload.description
      }
      api
        .editPlayer(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  savePlayer({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .savePlayer(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deletePlayer({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deletePlayer(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.PLAYERS](state, players) {
    state.players = players
  },
  [types.TOTAL_PLAYERS](state, value) {
    state.totalPlayers = value
  }
}

const state = {
  players: [],
  totalPlayers: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
